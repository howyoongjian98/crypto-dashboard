#  CoinGecko crypto dashboard assignment

```
TickerMinutely.where(coin_id: “bitcoin”).where(“timestamp > ?”, 3.months.ago)
```
1. Our application needs to plot a bitcoin price chart over 3 months. One of the developers used the following query to obtain the data needed to plot the chart.
After a period of time, our support team is getting complaints from our users that the chart is taking too long to load.
Suggest at least 4 possible ways to improve the performance and fix this bottleneck.

- Load and render a single high timeframe. Once component is loaded, make a request to get other small timeframes(to be rendered when user change timeframe).
- Use a better web host
- Use a CDN(load once and cache subsqeunt requests), provision more servers + load balancer, minify scripts
- Optimize db query, move to a NOSQL database as it's more suited for price data(querying speed, horizontal scaling)

1. You are a web developer working on a web application. Customer support approached you and escalated a problem that users are currently facing. The user visited the page at http://this-is-a-sample-site.com/shop/items and while the web page actually loads, the list of items for sale is stuck at loading. It seems to be related to Javascript. As a web developer, how do you diagnose this and the approach you would take to narrow down the problem?

- Open developer console, look for error messages or uncaught exceptions. Next look at the network tab, error could be caused by a failed API request. If there are no network errors, look at the response object. It could be empty and our code might only resolve loading if the length of returned object is truthy.

1. You have learnt languages like C and C++ in university. It is said that languages like C and C++ is powerful, much more powerful than languages like Java, Python, Ruby, etc.. Is it fair to say that all softwares should be written in C and C++? Why and why not?

- It's more powerful in the sense that it has more granular control over things. For example the C language does not have automatic garbage collection, which - if unhandled -  can cause memory leak. Higher level languages do not have to worry about 'issues' like this. Although if handled well, low level languages are more memory efficient. Another reason why low level languages are touted more 'powerful' is because they are usually compiled specifically for each OS. Meanwhile higher level languages use an interpreter to convert its code to machine code on the fly, which is less efficient. It's all about trade-offs. To emphasize my point, why shouldn't we write machine code as compared to C? It's much much more powerful.

1. Git: You are asked to work on a new existing project. When you browse the Git repo historical commits, you noticed many commits of small code changes and short description. Provide 3 advice you can give to the existing team to improve their Git practice?

- Try to bundle small commits into a single commit with a meaningful description. Instead of `lint js file`, `lint python files`, try `lint all files`
- Follow git commit methodologies like [Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/). Basically a single meaningful commit with a prefix and a description like `feat: bank transfer integration`
- When creating merge request, check the `squah commits` box and give it a commit message.
