import { remove, cloneDeep } from 'lodash'


export const state = () => ({
  coins: [],
  favourites: [],
  showOnlyFavourites: false
})

export const mutations = {
  loadCoins(state, data) {
    state.coins = data
  },
  addFavPropToCoins(state) {
    const coinsWithFavProp = state.coins.map(coin => {
      if (state.favourites.includes(coin.id)) {
        coin.fav = true
      } else {
        coin.fav = false
      }
      return coin
    })
    state.coins = coinsWithFavProp
  },
  toggleShowOnlyFavourites(state) {
    state.showOnlyFavourites = !state.showOnlyFavourites
  },
  updateFavCoins(state, coins) {
    state.favourites = coins
  }
}

export const actions = {
  async loadCoins({ commit, rootState }) {
    const currency = rootState.userSettings.defaultCurrency
    const result = await this.$axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&sparkline=true`)
    commit('loadCoins', result.data)
    commit('addFavPropToCoins')
  },
  async addToFavourite({ rootState, state, commit }, id) {
    let favourites = cloneDeep(state.favourites)

    if (!favourites.includes(id)) {
      favourites.push(id)
    } else {
      const remaining = remove(favourites, n => n !== id)
      favourites = remaining
    }

    const response = await this.$axios.patch(`https://vjnxyekryf.execute-api.ap-southeast-1.amazonaws.com/dev/updateFavourites/${rootState.auth.user.email}`, {
      favourites
    })

    commit('updateFavCoins', response.data.favourites)
    commit('addFavPropToCoins')
  },
  async fetchFavourites({commit, rootState}) {
    const response = await this.$axios.get(`https://vjnxyekryf.execute-api.ap-southeast-1.amazonaws.com/dev/favourites/${rootState.auth.user.email}`)
    commit('updateFavCoins', response.data.favourites)
    commit('addFavPropToCoins')
  }
}