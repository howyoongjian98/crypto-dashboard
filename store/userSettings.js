export const state = () => ({
  defaultCurrency: 'usd',
  currencyList: []
})

export const mutations = {
  setCurrencyList(state, data) {
    state.currencyList = data
  },
  setDefaultCurrency(state, data) {
    state.defaultCurrency = data
  }
}

export const actions = {
  async getCurrencies({ commit }) {
    const { data } = await this.$axios.get('https://api.coingecko.com/api/v3/simple/supported_vs_currencies')
    commit('setCurrencyList', data)
  }
}