import { find, findIndex, remove } from 'lodash'

export const state = () => ({
  cart: [],
  postCheckoutState: {}
})

export const getters = {
  total(state) {
    return state.cart.reduce((accumulator, item) => accumulator + item.subtotal, 0).toFixed(2)
  }
}

export const mutations = {
  removeFromCart(state, coin) {
    state.cart = remove(state.cart, (coinInCart)=> {
      return coinInCart.id !== coin
    })
  },
  addToCart(state, {coin, currency}) {
    // // make sure cart is unique
    const coinInState = find(state.cart, { id: coin.id })
    if (!coinInState) {
      // add into cart
      const price = coin.market_data.current_price[currency]
      state.cart.push({
        id: coin.id,
        image: coin.image,
        name: coin.name,
        price,
        quantity: 1,
        subtotal: price,
        marketData: coin.market_data.current_price
      })
    } else {
      // increment qty
      const index = state.cart.findIndex(i => i.id === coin.id)
      state.cart.splice(index, 1, {
        ...coinInState,
        quantity: parseFloat(coinInState.quantity) + 1,
        subtotal: coinInState.price * (coinInState.quantity + 1),
      })
    }
  },
  updateQty(state, { quantity, itemObj }) {
    const index = findIndex(this.state, { id: itemObj.id })
    state.cart.splice(index, 1, {
      ...itemObj,
      quantity,
      subtotal: quantity * itemObj.price
    })
  },
  recalculateSubtotal(state, newCurrency) {
    state.cart = state.cart.map(coin => {
      const newPrice = coin.marketData[newCurrency]
      coin.price = newPrice
      coin.subtotal = newPrice * coin.quantity
      return coin
    })
  },
  emptyCart(state) {
    state.cart = []
  },
  setPostCheckout(state, data) {
    state.postCheckoutState = data
  },
  clearPostCheckoutState(state) {
    state.postCheckoutState = {}
  }
}

export const actions = {
  addToCart({ rootState, commit }, coin) {
    commit('addToCart', {coin, currency: rootState.userSettings.defaultCurrency}, )
  },
  async checkout({state, getters, rootState, commit}) {
    const purchases = state.cart.map(item => {
      delete item['marketData']
      return item
    })
    const data = {
      purchases,
      email: rootState.auth.user.email,
      total: parseFloat(getters.total),
      currency: rootState.userSettings.defaultCurrency
    }
    const response = await this.$axios.post('https://vjnxyekryf.execute-api.ap-southeast-1.amazonaws.com/dev/purchase', data)
    if (response.status === 200) {
      response.data.message = `${response.data.message}. You may view your purchase on 'Order History' page`
      commit('emptyCart')
    }
    commit('setPostCheckout', {
      message: response.data.message,
      type: response.status === 200 ? 'success': 'error'
    })
  }
}